<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
  "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % languagedata SYSTEM "language.ent" > %languagedata;
  <!ENTITY % shareddata   SYSTEM "../release-notes.ent" > %shareddata;
]>

<chapter id="ch-installing" lang="en">
<title>Installation System</title>
<para>
The Debian Installer is the official installation system for Debian.  It offers
a variety of installation methods.  Which methods are available to install your
system depends on your architecture.
</para>
<para>
Images of the installer for &releasename; can be found together with
the Installation Guide on the <ulink url="&url-installer;">Debian
website</ulink>.
</para>
<para>
The Installation Guide is also included on the first CD/DVD of the official
Debian CD/DVD sets, at:
</para>
<screen>
/doc/install/manual/<replaceable>language</replaceable>/index.html
</screen>
<para>
You may also want to check
the <ulink url="&url-installer;index#errata">errata</ulink> for
debian-installer for a list of known issues.
</para>
<section id="inst-new">
<title>What's new in the installation system?</title>
<para>
There has been a lot of development on the Debian Installer since its previous
official release with &debian; &oldrelease;,
resulting in both improved hardware support and
some exciting new features.
</para>
<para>
In these Release Notes we'll only list the major changes in the
installer.  If you are interested in an overview of the detailed
changes since &oldreleasename;, please check the release announcements
for the &releasename; beta and RC releases available from the Debian
Installer's <ulink url="&url-installer-news;">news history</ulink>.
</para>

<section id="inst-changes">
<title>Major changes</title>

<!--

release-notes authors should check webwml repo or
http://www.debian.org/devel/debian-installer/

Sources (for Stretch):

https://www.debian.org/devel/debian-installer/News/2015/20150721
https://www.debian.org/devel/debian-installer/News/2015/20150815
https://www.debian.org/devel/debian-installer/News/2015/20150913
https://www.debian.org/devel/debian-installer/News/2015/20151026
https://www.debian.org/devel/debian-installer/News/2016/20160110
https://www.debian.org/devel/debian-installer/News/2016/20160521
https://www.debian.org/devel/debian-installer/News/2016/20160704
https://www.debian.org/devel/debian-installer/News/2016/20161112
https://www.debian.org/devel/debian-installer/News/2017/20170115

-->

<!-- TODO: Add
* 
-->

<variablelist>

<!-- The following empty paragraph purpose is to unb0rk
     the build until real material get committed -->

<varlistentry>
<term><!-- Empty Title --></term>
<listitem>
<para>
<!-- Empty Paragraph -->
</para>
</listitem>
</varlistentry>

<!-- new in Stretch-->
<varlistentry>
<term>Removed ports</term>
<listitem>
<para>
Support for the <literal>powerpc</literal> architecture has been removed.
</para>
</listitem>
</varlistentry>

<!-- new in Stretch-->
<varlistentry>
<term>New ports</term>
<listitem>
<para>
Support for the <literal>mips64el</literal> architecture has been added to the installer.
</para>
</listitem>
</varlistentry>

<!-- new in Stretch -->
<varlistentry arch="i386;amd64">
<term>Graphical installer</term>
<listitem>
<para>
The graphical installer is now the default on supported platforms.
The text installer is still accessible from the very first menu, or if
the system has limited capabilities.</para>
</listitem>
</varlistentry>

<!-- new in Stretch-->
<varlistentry arch="i386">
<term>The kernel flavor has been bumped to <literal>i686</literal></term>
<listitem>
<para>
The kernel flavor <literal>i586</literal> has been renamed to <literal>i686</literal>, since <literal>i586</literal> is no longer supported.
</para>
</listitem>
</varlistentry>

<!-- new in Stretch-->
<varlistentry>
<term>Desktop selection</term>
<listitem>
<para>
Since jessie, the desktop can be chosen within tasksel during installation,
and several desktops can be selected at the same time.</para>
</listitem>
</varlistentry>

<varlistentry>
<term>New languages</term>
<listitem>
<para>
Thanks to the huge efforts of translators, &debian; can now be installed in 75
languages, including English.
Most languages are available in both the text-based installation
user interface and the graphical user interface, while some
are only available in the graphical user interface. 
</para>

<!-- No new languages
<para>
Languages added in this release:
</para>

<itemizedlist>
<listitem>
<para>
</para>
</listitem>
</itemizedlist>
-->

<para>
The languages that can only be selected using the graphical installer as their
character sets cannot be presented in a non-graphical environment are: Amharic,
Bengali, Dzongkha, Gujarati, Hindi, Georgian, Kannada, Khmer, Malayalam,
Marathi, Nepali, Punjabi, Tamil, Telugu, Tibetan, and Uyghur.
</para>
</listitem>
</varlistentry>

</variablelist>
</section>

<section id="inst-auto">
<title>Automated installation</title>
<para>
Some changes mentioned in the previous section also imply changes in
the support in the installer for automated installation using preconfiguration
files.  This means that if you have existing preconfiguration files that worked
with the &oldreleasename; installer, you cannot expect these to work with the new
installer without modification.
</para>
<para>
The <ulink
url="&url-install-manual;">Installation
Guide</ulink> has an updated separate appendix with extensive documentation on using
preconfiguration.
</para>
</section>
</section>

</chapter>

