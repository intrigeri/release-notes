<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
  "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % languagedata SYSTEM "language.ent" > %languagedata;
  <!ENTITY % shareddata   SYSTEM "../release-notes.ent" > %shareddata;
]>

<chapter id="ch-information" lang="en">
<title>Issues to be aware of for &releasename;</title>

<para>
Sometimes, changes introduced in a new release have side-effects
we cannot reasonably avoid, or they expose
bugs somewhere else. This section documents issues we are aware of.  Please also
read the errata, the relevant packages' documentation, bug reports, and other
information mentioned in <xref linkend="morereading"/>.
</para>

<section id="upgrade-specific-issues">
  <title>Upgrade specific items for &releasename;</title>
  <para>
    This section covers items related to the upgrade from
    &oldreleasename; to &releasename;.
  </para>

  <section id="isa-baseline-for-s390x" arch="s390x">
    <!-- stretch to buster-->
    <title>s390x ISA raised to z196</title>
    <para>
      The baseline ISA of the <literal>s390x</literal> port has been raised
      to z196. This enables Debian to support packages like <systemitem
      role="package">rust</systemitem>, <systemitem
      role="package">go</systemitem> and <systemitem
      role="package">nodejs</systemitem>, which do not support older ISAs.
    </para>
    <para>
      As a consequence people should not upgrade to buster if they have an
      older CPU.
    </para>
  </section>

  <section id="hidepid-unsupported">
    <!-- stretch to buster-->
    <title>Hidepid mount options for procfs unsupported</title>
    <para>
      The hidepid mount options for <filename>/proc</filename> are known to cause
      problems with current versions of systemd, and are considered by systemd
      upstream to be an unsupported configuration. Users who have modified
      <filename>/etc/fstab</filename> to enable these options are advised to
      disable them before the upgrade, to ensure login sessions work on
      &releasename;. (A possible route to re-enabling them is outlined on the
      wiki's <ulink
      url="https://wiki.debian.org/Hardening#Mounting_.2Fproc_with_hidepid">Hardening</ulink>
      page.)
    </para>
  </section>

  <section id="noteworthy-obsolete-packages" condition="fixme">
    <title>Noteworthy obsolete packages</title>
    <para>
      The following is a list of known and noteworthy obsolete
      packages (see <xref linkend="obsolete"/> for a description).
    </para>
    <!-- TODO:
         - Use the change-release information and sort by popcon

         This needs to be reviewed based on real upgrade logs (jfs)

         Alternative, another source of information is the UDD
         'not-in-testing' page:
         http://udd.debian.org/bapase.cgi?t=testing
    -->
    <para>
      The list of obsolete packages includes:
      <itemizedlist>
        <listitem>
          <para>
            The package <systemitem role="package">mcelog</systemitem> is no
            longer supported in kernels above 4.12. <systemitem
            role="package">rasdaemon</systemitem> can be used as its
            replacement.
          </para>
        </listitem>
        <listitem>
          <para>
            The package <systemitem role="package">revelation</systemitem>,
            which is used to store passwords, is not included in &releasename;.
            <systemitem role="package">keepass2</systemitem> can import
            previously exported password XML files from <systemitem
            role="package">revelation</systemitem>. Please make sure you export
            your data from revelation before upgrading, to avoid losing access
            to your passwords.
          </para>
        </listitem>
        <listitem>
          <para>
            The package <systemitem role="package">phpmyadmin</systemitem>
            is not included in &releasename;.
          </para>
        </listitem>
      </itemizedlist>
    </para>
  </section>
  <section id="deprecated-components" condition="fixme">
    <title>Deprecated components for &releasename;</title>
    <para>
      With the next release of &debian; &nextrelease; (codenamed
      &nextreleasename;) some features will be deprecated. Users
      will need to migrate to other alternatives to prevent
      trouble when updating to &debian; &nextrelease;.
    </para>
    <para>
      This includes the following features:
    </para>

    <itemizedlist>
      <listitem>
        <para>
          Python 2 will stop being supported by its upstream on <ulink
          url="https://www.python.org/dev/peps/pep-0373/">January 1,
          2020</ulink>. &debian; hopes to drop <systemitem
          role="package">python-2.7</systemitem> for &nextrelease;. If users
          have functionality that relies on <command>python</command>, they
          should prepare to migrate to <command>python3</command>.
        </para>
      </listitem>
      <listitem>
        <para>
          Icinga 1.x is EOL upstream since 2018-12-31, while the
          <systemitem role="package">icinga</systemitem> package
          is still present, users should use the buster lifetime
          to migrate to Icinga 2
          (<systemitem role="package">icinga2</systemitem> package)
          and Icinga Web 2
          (<systemitem role="package">icingaweb2</systemitem>
          package). The
          <systemitem role="package">icinga2-classicui</systemitem>
          package is still present to use the Icinga 1.x CGI web
          interface with Icinga 2, but the support for it will be
          removed in Icinga 2.11. Icinga Web 2 should be used
          instead.
        </para>
      </listitem>
      <listitem>
        <para>
          The Mailman mailing list manager suite version 3 is newly available
          in this release. Mailman has been split up into various components;
          the core is available in the package <systemitem
          role="package">mailman3</systemitem> and the full suite can be
          obtained via the <systemitem
          role="package">mailman3-full</systemitem> metapackage.
        </para>
        <para>
          The legacy Mailman version 2.1 remains available in this release in
          the package <systemitem role="package">mailman</systemitem>, so you
          can migrate any existing installations at your own pace.  The Mailman
          2.1 package will be kept in working order for the foreseeable future,
          but will not see any major changes or improvements. It will be
          removed from the first Debian release after Mailman upstream has
          stopped support for this branch.
        </para>
        <para>
          Everyone is encouraged to upgrade to Mailman 3, the modern release
          under active development.
        </para>
      </listitem>

    </itemizedlist>
  </section>
  <section id="before-first-reboot">
    <title>Things to do post upgrade before rebooting</title>
    <!-- If there is nothing to do -->
    <para>
      When <literal>apt dist-upgrade</literal> has finished, the
      <quote>formal</quote> upgrade is complete.  For the upgrade to
      &releasename;, there are no special actions needed before
      performing a reboot.
    </para>
    <!-- If there is something to do -->
    <para condition="fixme">
      When <literal>apt dist-upgrade</literal> has finished, the <quote>formal</quote> upgrade
      is complete, but there are some other things that should be taken care of
      <emphasis>before</emphasis> the next reboot.
    </para>

    <programlisting condition="fixme">
      add list of items here
      <!--itemizedlist>
        <listitem>
          <para>
            bla bla blah
          </para>
        </listitem>
      </itemizedlist-->
    </programlisting>

  </section>
  <section id="obsolete-sysvinit-packages">
    <!-- stretch to buster -->
    <title>SysV init related packages no longer required</title>
    <note>
      <para>
        This section does not apply if you decided to stick with sysvinit-core.
      </para>
    </note>
    <para>
      After the switch to systemd as default init system in Jessie and further refinements
      in Stretch, various SysV related packages are no longer required and can now be
      purged safely via
      <screen>apt purge initscripts sysv-rc insserv startpar</screen>
    </para>
  </section>
</section>

<section id="limited-security-support">
  <title>Limitations in security support</title>
  <para>
    There are some packages where Debian cannot promise to provide
    minimal backports for security issues.  These are covered in the
    following subsections.
  </para>
  <para>
    Note that the package <systemitem
    role="package">debian-security-support</systemitem> helps to track
    the security support status of installed packages.
  </para>

  <section id="browser-security" condition="fixme">
    <!-- keep as a regular (?) -->
    <title>Security status of web browsers</title>
    <para>
      Debian &release; includes several browser engines which are
      affected by a steady stream of security vulnerabilities. The
      high rate of vulnerabilities and partial lack of upstream
      support in the form of long term branches make it very difficult
      to support these browsers with backported security fixes.
      Additionally, library interdependencies make it impossible to
      update to newer upstream releases. Therefore, browsers built
      upon the webkit, qtwebkit and khtml engines are included in
      &releasename;, but not covered by security support.  These
      browsers should not be used against untrusted websites.
    </para>
    <para>
      For general web browser use we recommend Firefox or Chromium.
    </para>
    <para>
      Chromium - while built upon the Webkit codebase - is a leaf
      package, which will be kept up-to-date by rebuilding the current
      Chromium releases for stable.  Firefox and Thunderbird will also
      be kept up-to-date by rebuilding the current ESR releases for
      stable.
    </para>
  </section>

  <section id="libv8" condition="fixme">
    <!-- jessie to stretch -->
    <title>Lack of security support for the ecosystem around libv8 and
      Node.js (FIXME; still relevant?)</title>
    <para>
      The Node.js platform is built on top of <systemitem
      role="package">libv8-3.14</systemitem>, which experiences a high
      volume of security issues, but there are currently no volunteers
      within the project or the security team sufficiently interested
      and willing to spend the large amount of time required to stem
      those incoming issues.
    </para>
    <para>
      Unfortunately, this means that <systemitem
      role="package">libv8-3.14</systemitem>, <systemitem
      role="package">nodejs</systemitem>, and the associated node-*
      package ecosystem should not currently be used with untrusted
      content, such as unsanitized data from the Internet.
    </para>
    <para>
      In addition, these packages will not receive any security
      updates during the lifetime of the &releasename; release.
    </para>
</section>

</section>

<section id="package-specific-issues">
  <title>Package specific issues</title>
  <para>
    In most cases, packages should upgrade smoothly between
    &oldreleasename; and &releasename;.  There are a small number of
    cases where some intervention may be required, either before or
    during the upgrade; these are detailed below on a per-package
    basis.
  </para>

  <section id="glibc-and-linux" arch="i386;amd64">
    <!-- stretch to buster-->
    <title>Glibc requires Linux kernel 3.2 or higher</title>
    <para>
      Starting with <systemitem role="source">glibc</systemitem> 2.26, Linux
      kernel 3.2 or later is required. To avoid completely breaking the system,
      the preinst for <systemitem role="package">libc6</systemitem> performs a
      check. If this fails, it will abort the package installation, which will
      leave the upgrade unfinished.  If the system is running a kernel older
      than 3.2, please update it before starting the distribution upgrade.
    </para>
  </section>

  <section id="su-environment-variables">
    <!-- stretch to buster-->
    <title>Semantics for using environment variables for su changed</title>
    <para>
      <literal>su</literal> changed semantics in &releasename; and no longer copies
      over <literal>DISPLAY</literal> and <literal>XAUTHORITY</literal>
      environment variables. You need to explicitly set them and allow
      explicitly access to your display if you need to run graphical
      applications with <literal>su</literal>. See <ulink
      url="&url-bts;905409">bug #905409</ulink> for an extensive discussion.
    </para>
  </section>

</section>

</chapter>
